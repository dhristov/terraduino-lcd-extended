#include <DHT.h>

#include <elapsedMillis.h>

#include <DS3231.h>
#include <Wire.h>
//Json Library
#include <ArduinoJson.h>

#include <Ethernet.h>
#include <SPI.h>

#include <LiquidCrystal.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // RESERVED MAC ADDRESS
EthernetClient client;

const String module_name = "module3";

DHT dht1;
DHT dht2;

DS3231 clock;
RTCDateTime dt;

// initialize the library with the numbers of the interface pins

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

elapsedMillis timer0;
const long interval = 10000; // READING INTERVAL

int connected_sensors = 0;
float t1 = 0;  // TEMPERATURE VAR
float h1 = 0; // HUMIDITY VAR
float t2 = 0;
float h2 = 0;

int nc = 0; //Network connection simulation INI


byte temp[8] = { //icon for thermometer

  B00100,
  B01010,
  B01010,
  B01110,
  B01110,
  B11111,
  B11111,
  B01110
};

byte humi[8] = { //icon for water droplet
  B00100,
  B00100,
  B01010,
  B01010,
  B10001,
  B10001,
  B10001,
  B01110,
};

byte sconnect[8] = { //icon for connection
  0b00000,
  0b11111,
  0b10001,
  0b10001,
  0b11111,
  0b00100,
  0b11111,
  0b00000
};

void setup() {
  Serial.begin(9600);
  clock.begin();
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  Serial.println("Terrarium Serial");
  // Print a message to the LCD.
  lcd.print("Weather v0.1");
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print("Sensor loading...");

  lcd.clear();
  lcd.print("Network check...");

  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP"); //There is a problem with DHCP
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("DHCP ERROR");
    lcd.setCursor(0, 1);
    lcd.print("Please Restart");
    for (;;)
      ;
  }
  nc = 1;

  delay(2000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(Ethernet.localIP());
  lcd.setCursor(0, 1);
  lcd.println("Ready!");


  Serial.println(F("Connected!"));
  Serial.println(Ethernet.localIP());

  delay(4000);
  lcd.clear();

  lcd.createChar(1, temp);
  lcd.createChar(2, humi);
  lcd.createChar(3, sconnect);

  dht1.setup(15); 
  dht2.setup(16);

  h1 = dht1.getHumidity();
  t1 = dht1.getTemperature();

  h2 = dht2.getHumidity();
  t2 = dht2.getTemperature();

  delay(2000);

  //Display the statics on the display
  lcd.setCursor(2, 0);
  lcd.print(module_name);
  lcd.setCursor(9, 1);
  lcd.write(2);
  lcd.setCursor(15, 1);
  lcd.print("%");
  lcd.setCursor(2, 1);
  lcd.write(1);
  lcd.setCursor(8, 1);
  lcd.print((char)223);

  timer0 = 0; // clear the timer at the end of startup

}
void CheckSensors() {

  if (timer0 >= interval) { // READ ONLY ONCE PER INTERVAL
    timer0 -= interval;

    dt = clock.getDateTime(); //get current time
    
    h1 = dht1.getHumidity();
    t1 = dht1.getTemperature();

    h2 = dht2.getHumidity();
    t2 = dht2.getTemperature();

    DataSend(); //send data
  }
}

//Print the changeable values on the display
void LcdPint() {
  lcd.setCursor(0, 0);
  if (nc == 1) {
    lcd.write(3);
  } else if (nc == 0) {
    lcd.print(" ");
  } else if (nc == 2) {
    lcd.print("E");
  }

  lcd.setCursor(10, 1);
  lcd.print(h1);

  lcd.setCursor(3, 1);
  lcd.print(t1);

}

//Construct the JSON response
JsonObject& prepareResponse(JsonBuffer& jsonBuffer, String measured_at) {

  JsonObject& root = jsonBuffer.createObject();
  root["modulename"] = module_name;

  JsonObject& environmentsObject = root.createNestedObject("environments");


    JsonObject& environmentsOne = environmentsObject.createNestedObject("1"); 
    environmentsOne["temperature"] = t1;
    environmentsOne["humidity"] = h1;
    environmentsOne["measured_at"] =  measured_at;

    JsonObject& environmentsTwo = environmentsObject.createNestedObject("2");
    environmentsTwo["temperature"] = t2;
    environmentsTwo["humidity"] = h2;
    environmentsTwo["measured_at"] =  measured_at;

  return root;
}

void writeResponse(EthernetClient& client, JsonObject& json) {

  char buffer[256];
  json.printTo(buffer, sizeof(buffer));

  if (client.connect("api.terrariums.eu", 80)) { // REPLACE WITH YOUR SERVER ADDRESS
    client.println("POST /v1/sensors HTTP/1.1");
    client.println("Host: api.terrariums.eu"); // SERVER ADDRESS HERE TOO
    client.println("Content-Type: application/json");
    client.println("Authorization: Basic bW9kdWxlMzoxMzIxMzI=");
    client.println("Connection: close");
    client.print("Content-Length: ");
    client.println(strlen(buffer));
    client.println();
    client.println(buffer);

    nc = 1;
  } else {
    nc = 2;
    Serial.println("connection failed");
  }

  while (client.connected())
  {
    if ( client.available() )
    {
      char c = client.read();
      Serial.print(c);
    }
  }
  Serial.println();
  client.stop();
  //End  post request

}
void DataSend() {

	//This is the part that I think crashes my entire code. I tried using the helper with the RTC library that I use
	//but that made things worse.
	
    String measured_at = (String) dt.year + "-" + dt.month + "-" + dt.day  + " " + dt.hour   + ":" + dt.minute + ":" + dt.second;

    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& json = prepareResponse(jsonBuffer, measured_at);
    json.prettyPrintTo(Serial);

    writeResponse(client, json);

}

void loop() {
  CheckSensors();
  LcdPint();
}
